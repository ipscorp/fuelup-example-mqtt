## Install
Create dir - `certs`
Add certs:
* cert.pem
* private.pem
* rootCA.crt

## Build
`gcc subscription.c -o subscription.out -LMQTTlib/lib/ -lpaho-mqtt3cs`
`gcc publication.c -o publication.out -LMQTTlib/lib/ -lpaho-mqtt3cs`

## Run
`./subscription.out`
`./publication.out`